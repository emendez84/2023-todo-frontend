import Vue from 'vue'
import VueRouter from 'vue-router'
import Ping from '../components/Hello.vue'
import Task from '../components/Task.vue'


Vue.use(VueRouter)

const routes = [
  {
    path: '/ping',
    name: 'Ping',
    component: Ping
  },
  {
    path: '/',
    name: 'Task',
    component: Task
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
